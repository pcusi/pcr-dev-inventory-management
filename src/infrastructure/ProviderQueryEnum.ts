export enum ProviderQueryEnum {
    INSERT = '(names, lastnames, documentType, documentNumber, email, createdAt, idEmployee) VALUES (?, ?, ?, ?, ?, ?, ?)',
    SELECT = '* FROM Provider',
    COUNT = 'count(id) as count FROM Provider',
    SELECT_BY_ID = '* FROM Provider WHERE id = ?',
    UPDATE = 'Provider SET firstName = ?, lastName = ?, identityCard = ?, updatedAt = ? WHERE id = ?',
}