export enum CustomerQueryEnum {
    INSERT = '(names, lastnames, documentType, documentNumber, email, createdAt, idEmployee) VALUES (?, ?, ?, ?, ?, ?, ?)',
    SELECT = '* FROM Customer',
    COUNT = 'COUNT(*) as count FROM Customer'
}