export enum QueryEnum {
    INSERT = 'INSERT INTO',
    SELECT = 'SELECT',
    UPDATE = 'UPDATE',
    DELETE = 'DELETE FROM',
}

export const invokeQuery = (queryType: string, queryTable?: string | null, queryValues?: string): string => {
    switch (queryType) {
        case 'INSERT':
            return `${QueryEnum.INSERT} ${queryTable} ${queryValues}`;
        case 'SELECT':
            return `${QueryEnum.SELECT} ${queryValues}`;
        case 'UPDATE':
            return `${QueryEnum.UPDATE} ${queryValues}`;
        case 'DELETE':
            return `${QueryEnum.DELETE} ${queryValues}`;    
        default:
            return `${QueryEnum.SELECT} ${queryValues}`;
    }
}