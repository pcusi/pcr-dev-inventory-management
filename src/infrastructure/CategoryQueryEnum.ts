export enum CategoryQueryEnum {
    INSERT = '(name) VALUES (?)',
    SELECT = '* FROM Category',
    SELECT_BY_ID = '* FROM Category WHERE id = ?',
    SELECT_BY_NAME = 'id FROM Category WHERE name = ?',
    UPDATE = 'Category SET name = ? WHERE id = ?',
}