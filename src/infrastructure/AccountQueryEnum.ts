export enum AccountQueryEnum {
    INSERT = '(username, password, createdAt, idEmployee) VALUES (?, ?, ?, ?)',
    SELECT = '* FROM Account',
    SELECT_BY_USERNAME = 'a.password, a.username, a.id as idAccount, e.* FROM Account a INNER JOIN Employee e ON a.idEmployee = e.id where a.username = ?',
    SELECT_BY_ID = '* FROM Account WHERE id = ?',
    UPDATE = 'Account SET username = ?, password = ?, updatedAt = ? WHERE id = ?',
}