export interface QueryErrorEnum {
    code?: string;
    sqlMessage?: string;
    sqlState?: string;
    index?: number;
    sql?: string;
}