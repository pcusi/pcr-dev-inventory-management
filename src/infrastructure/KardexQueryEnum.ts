export enum KardexQueryEnum {
    INSERT = '(kardexCode, kardexType, kardexOrder, kardexTotal, kardexDate, idEmployee) VALUES (?, ?, ?, ?, ?, ?)',
    SELECT = '* FROM Kardex',
    SELECT_BY_ID = '* FROM Kardex WHERE id = ?',
    UPDATE = 'Stock SET stockName = ? WHERE id = ?',
    COUNT = 'COUNT(id) AS count FROM Kardex',
    COUNT_BY_ENTRY = 'Count(id) AS count FROM Kardex WHERE kardexType = "Entrada"',
    COUNT_BY_EXIT = 'Count(id) AS count FROM Kardex WHERE kardexType = "Salida"'
}