export enum ProductQueryEnum {
    INSERT = '(title, createdAt, idStock, idCategory, idAccount) VALUES (?, ?, ?, ?, ?)',
    SELECT = 'p.title, s.stockName, c.categoryName FROM Product p INNER JOIN Stock s ON p.idStock = s.id INNER JOIN Category c ON p.idCategory = c.id;',
    SELECT_BY_ID = '* FROM Product WHERE id = ?',
    MULTIPLE_INSERT = '(name, createdAt, idStock, idCategory, idEmployee) VALUES ?',
    UPDATE = 'Product SET title = ?, description = ?, purchasePrice = ?, salePrice = ?, stock = ?, updatedAt = ? WHERE id = ?',
    UPDATE_STOCK = 'Product SET stock = ? WHERE id = ?',
    UPDATE_PRICE = 'Product SET price = ? WHERE id = ?',
    SELECT_PRODUCT_STOCK = "title, stock from Product WHERE id = ?"
}