export enum StockQueryEnum {
    INSERT = '(name) VALUES (?)',
    SELECT = '* FROM Stock',
    SELECT_BY_ID = '* FROM Stock WHERE id = ?',
    SELECT_BY_NAME = 'id FROM Stock WHERE name = ?',
    UPDATE = 'Stock SET name = ? WHERE id = ?',
}