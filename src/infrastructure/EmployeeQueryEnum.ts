export enum EmployeeQueryEnum {
    INSERT = '(role, names, lastnames, documentType, documentNumber, email, password, createdAt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
    SELECT = '* FROM Employee',
    SELECT_BY_ID = '* FROM Employee WHERE id = ?',
    SELECT_BY_USERNAME = '* FROM Employee WHERE email = ?',
    UPDATE = 'Employee SET firstName = ?, lastName = ?, identityCard = ?, updatedAt = ? WHERE id = ?',
}