import { QueryErrorEnum } from "src/infrastructure/QueryErrorEnum";
import { Request, Response, Router } from "express";
import * as ensure from "../middleware/AuthenticationMiddleware";
import * as moment from "moment";
import { KardexResponse } from "types/kardex_schema";
import {countKardex, createKardex, getKardex, getKardexes, syncKardexProduct} from "../controllers/KardexController";
import {updateProductPrice, updateProductStock} from "../controllers/ProductController";
const router = Router();

type KardexOrderProductResponse = {
    unitaryPrice: number;
    quantity: number;
    idProduct?: number;
    salePrice?: number;
}

type KardexProductResponse = {
    title?: string;
    stockName?: string;
    categoryName?: string;
}

type KardexOrderResponse = {
    product?: KardexProductResponse,
    quantity?: number,
    unitaryPrice?: number,
    salePrice?: number
}

type KardexStoredResponse = {
    kardexOrder: KardexOrderResponse[],
    kardexCode?: string,
    kardexType?: string,
    kardexTotal?: number,
    kardexDate?: string
}

router.post("/kardex", ensure.auth, async (req: Request, res: Response) => {
    const kardex: KardexResponse = req.body;

    const count = await countKardex();

    kardex.idEmployee = req.sub;
    kardex.kardexDate = moment().unix();
    kardex.kardexCode = `IVPRK-00` + (Number(count) + 1);
    kardex.kardexOrder = JSON.stringify(kardex.kardexOrderTMP);
    kardex.kardexTotal = Number(kardex.kardexOrderTMP?.reduce((total, item) => { return total + (item.unitaryPrice * item.quantity) }, 0));

    kardex.kardexOrderTMP?.map((order: KardexOrderProductResponse) => updateProductStock(order.quantity!, order.idProduct!));
    kardex.kardexOrderTMP?.map((order: KardexOrderProductResponse) => updateProductPrice(order.salePrice!, order.idProduct!));

    createKardex(kardex, (error: QueryErrorEnum, kardexId: number) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ kardexId });
    });
});

router.get("/kardex/:id", ensure.auth, (req: Request, res: Response) => {
    const kardexIx: number = Number(req.params.id);

    getKardex(kardexIx, async (error: QueryErrorEnum, kardex: KardexResponse) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        const kardex_order_parse: KardexOrderProductResponse[] = JSON.parse(kardex.kardexOrder as string);

        const kardex_order = await Promise.all(kardex_order_parse.map(async (kardex: KardexOrderProductResponse) => {

            const product: KardexProductResponse = await syncKardexProduct(kardex.idProduct as number) as KardexProductResponse;
            delete kardex.idProduct;

            return {
                product,
                ...kardex,
            } as KardexOrderResponse
        }));

        let kardex_clean_parse: KardexStoredResponse = {
            kardexDate: moment.unix(kardex.kardexDate as number).format("DD/MM/YYYY"),
            kardexCode: kardex.kardexCode,
            kardexType: kardex.kardexType,
            kardexTotal: kardex.kardexTotal,
            kardexOrder: [...kardex_order],
        };

        return res.status(200).json({ kardex: kardex_clean_parse });
    });
});

router.get("/kardexes", ensure.auth, (req: Request, res: Response) => {
    getKardexes((error: QueryErrorEnum, kardexes: KardexResponse[]) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ kardexes });
    });
});

export { router };