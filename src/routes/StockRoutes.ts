import { QueryErrorEnum } from "../infrastructure/QueryErrorEnum";
import { Request, Response, Router } from "express";
import { StockResponse } from "types/stock_schema";
import { createStock, getStocks } from "../controllers/StockController";
const router = Router();

router.post("/stock", async (req: Request, res: Response) => {
    const stock: StockResponse = req.body;

    createStock(stock, (error: QueryErrorEnum, stockId: number) => {
        if (error) return res.status(400).json({
            "sqlMessage": error.sqlMessage,
            "sql": error.sql,
        });

        res.status(200).json({ stockId });
    });
});

router.get("/stocks", async (req: Request, res: Response) => {
    getStocks((error: QueryErrorEnum, stocks: StockResponse[]) => {
        if (error) return res.status(400).json({
            "sqlMessage": error.sqlMessage,
            "sql": error.sql,
        });

        res.status(200).json({ stocks });
    });
});

export { router };