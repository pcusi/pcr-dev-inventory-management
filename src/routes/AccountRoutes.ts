import { getUser, logIn } from "../controllers/AccountController";
import { QueryErrorEnum } from "../infrastructure/QueryErrorEnum";
import { Request, Response, Router } from "express";
import * as ensure from "../middleware/AuthenticationMiddleware";
import { EmployeeResponse } from "types/employee_schema";
const router = Router();

router.post("/account/login", async (req: Request, res: Response) => {
  const account: EmployeeResponse = req.body;

  logIn(account, (error: QueryErrorEnum, token: string) => {
    if (token === "not-found")
      return res.status(400).json({
        message: "Account not found",
      });

    if (token === "not-match")
      return res.status(400).json({
        message: "Passwords doesn't match",
      });

    if (error)
      return res.status(400).json({
        sqlMessage: error.sqlMessage,
        sql: error.sql,
      });

    res.status(200).json({ token });
  });
});

router.get("/account/user", ensure.auth, (req: Request, res: Response) => {
  getUser(req.headers.authorization!, (error: any, user: EmployeeResponse) => {
    return res.status(200).json(user);
  });
});

export { router };
