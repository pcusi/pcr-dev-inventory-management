import { QueryErrorEnum } from "../infrastructure/QueryErrorEnum";
import { Request, Response, Router } from "express";
import { CategoryResponse } from "types/category_schema";
import { createCategory, getCategories } from "../controllers/CategoryController";
const router = Router();

router.post("/category", async (req: Request, res: Response) => {
    const category: CategoryResponse = req.body;

    createCategory(category, (error: QueryErrorEnum, categoryId: number) => {
        if (error) return res.status(400).json({
            "sqlMessage": error.sqlMessage,
            "sql": error.sql,
        });

        res.status(200).json({ categoryId });
    });
});

router.get("/categories", async (req: Request, res: Response) => {
    getCategories((error: QueryErrorEnum, categories: CategoryResponse[]) => {
        if (error) return res.status(400).json({
            "sqlMessage": error.sqlMessage,
            "sql": error.sql,
        });

        res.status(200).json({ categories });
    });
});

export { router };