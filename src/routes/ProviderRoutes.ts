import {
    createProvider,
    importExcelProvider,
    exportExcelProvider,
    getProvider
} from "../controllers/ProviderController";
import { QueryErrorEnum } from "src/infrastructure/QueryErrorEnum";
import { Request, Response, Router } from "express";
import * as ensure from "../middleware/AuthenticationMiddleware";
import { ProviderResponse } from "types/provider_schema";
import * as moment from "moment";
const router = Router();

router.post("/provider", ensure.auth, async (req: Request, res: Response) => {
    const provider: ProviderResponse = req.body;

    provider.idEmployee = req.sub;
    provider.createdAt = moment().unix();

    await createProvider(provider, (error: QueryErrorEnum, providerId: number) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ providerId });
    });
});

router.get("/provider/:id", ensure.auth, async (req: Request, res: Response) => {
    const providerId: number = Number(req.params.id);

    await getProvider(providerId, (error: QueryErrorEnum, provider: ProviderResponse) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ provider });
    });
});

router.post("/provider/excel/import", ensure.auth, async (req: Request, res: Response) => {
    importExcelProvider(req, (error: any, file: any) => {
        if (error) return res.status(400).send({ error });

        return res.status(200).send({ file });
    });
});

router.get("/provider/excel/export", ensure.auth, async (req: Request, res: Response) => {
    await exportExcelProvider((error: any, message: string) => {
        if (error) return res.status(400).send({ error });

        return res.status(200).send({ message });
    });
});


export { router };