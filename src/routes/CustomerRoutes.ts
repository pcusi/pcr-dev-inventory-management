import { QueryErrorEnum } from "../infrastructure/QueryErrorEnum";
import { Request, Response, Router } from "express";
import * as ensure from "../middleware/AuthenticationMiddleware";
import * as moment from "moment";
import { createCustomer, getCustomers } from "../controllers/CustomerController";
import { CustomerResponse } from "types/customer_schema";
const router = Router();

router.post("/customer", ensure.auth, async (req: Request, res: Response) => {
    const customer: CustomerResponse = req.body;

    customer.createdAt = moment().unix();
    customer.idEmployee = req.sub;

    createCustomer(customer, (error: QueryErrorEnum, customerId: number) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ customerId });
    });
});

router.get("/customers", ensure.auth, (req: Request, res: Response) => {
    getCustomers((error: QueryErrorEnum, customers: CustomerResponse[]) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ customers });
    });
});

export { router };