import { createEmployee, getEmployee, getEmployees, updateEmployee } from "../controllers/EmployeeController";
import { EmployeeResponse } from "types/employee_schema";
import { QueryErrorEnum } from "src/infrastructure/QueryErrorEnum";
import { Request, Response, Router } from "express";
import * as ensure from "../middleware/AuthenticationMiddleware";
import * as bcrypt from 'bcrypt';
const router = Router();

router.post("/employee", ensure.auth, async (req: Request, res: Response) => {
    const employee: EmployeeResponse = req.body;

    employee.password = await bcrypt.hash(employee.password!, 10);

    createEmployee(employee, (error: QueryErrorEnum, employeeId: number) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage });

        res.status(200).json({ "employeeId": employeeId });
    });
});

router.get("/employees", ensure.auth, async (req: Request, res: Response) => {
    getEmployees((error: QueryErrorEnum, employees: EmployeeResponse[]) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage });

        res.status(200).json({ employees });
    });
});

router.get("/employee/:id", async (req: Request, res: Response) => {
    const employeeId: number = Number(req.params.id);

    getEmployee(employeeId, (error: QueryErrorEnum, employee: EmployeeResponse) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage });

        res.status(200).json({ employee });
    });
});

router.patch("/employee/:id", async (req: Request, res: Response) => {
    const employee: EmployeeResponse = req.body;

    const employeeId: number = Number(req.params.id);

    updateEmployee(employeeId, employee, (error: QueryErrorEnum, message: string) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage });

        res.status(200).json({ message });
    });
});

export { router };