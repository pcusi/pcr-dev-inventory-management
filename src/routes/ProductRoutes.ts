import { QueryErrorEnum } from "src/infrastructure/QueryErrorEnum";
import { Request, Response, Router } from "express";
import * as ensure from "../middleware/AuthenticationMiddleware";
import * as moment from "moment";
import { ProductResponse } from "types/product_schema";
import { createProduct, getProduct, getProducts, importExcelProduct } from "../controllers/ProductController";
const router = Router();

router.post("/product", ensure.auth, async (req: Request, res: Response) => {
    const product: ProductResponse = req.body;

    product.idEmployee = req.sub;
    product.createdAt = moment().unix();

    createProduct(product, (error: QueryErrorEnum, productId: number) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ productId });
    });
});

router.get("/products", ensure.auth, async (req: Request, res: Response) => {
    getProducts((error: QueryErrorEnum, products: ProductResponse[]) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ products });
    });
});

router.get("/product/:id", ensure.auth, async (req: Request, res: Response) => {
    const productId: number = Number(req.params.id);

    getProduct(productId, (error: QueryErrorEnum, product: ProductResponse) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ product });
    });
});

router.post("/product/excel/import", ensure.auth, async (req: Request, res: Response) => {
    importExcelProduct(req, (error: QueryErrorEnum, message: string) => {
        if (error) return res.status(400).json({ "sqlMessage": error.sqlMessage, "sql": error.sql, });

        return res.status(200).json({ message });
    });
});

export { router };