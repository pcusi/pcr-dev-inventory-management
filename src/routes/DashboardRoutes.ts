
import { Request, Response, Router } from "express";
import * as ensure from "../middleware/AuthenticationMiddleware";
import {report} from "../controllers/DashboardController";
const router = Router();

router.get("/dashboard/report", ensure.auth, async (req: Request, res: Response) => {
    await report((error: any, response: object) => {
        if (error) return res.status(400).send({ error });

        return res.status(200).send({ report: response });
    });
});


export { router };