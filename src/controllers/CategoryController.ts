import { conn } from "../config/Database";
import { CategoryResponse } from "types/category_schema";
import { invokeQuery } from "../infrastructure/QueryEnum";
import { CategoryQueryEnum } from "../infrastructure/CategoryQueryEnum";

export const createCategory = (category: CategoryResponse, callback: Function) => {
    conn.query(invokeQuery('INSERT', 'Category', CategoryQueryEnum.INSERT), [category.name], (error: any, results: any) => {
        if (error) return callback(error);

        const categoryId: number = results['insertId'];

        return callback(null, categoryId);
    });
}

export const getCategories = (callback: Function) => {
    conn.query(invokeQuery('SELECT', null, CategoryQueryEnum.SELECT), (error: any, results: any) => {
        if (error) return callback(error);

        const categories: CategoryResponse[] = results;

        return callback(null, categories);
    });
}

export const getCategory = (categoryId: number, callback: Function) => {
    conn.query(invokeQuery('SELECT', null, CategoryQueryEnum.SELECT_BY_ID),
        [categoryId], (error: any, results: any) => {
            if (error) return callback(error);

            return callback(null, results[0]);
        });
}