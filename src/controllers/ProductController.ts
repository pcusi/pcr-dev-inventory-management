import * as xlsx from "xlsx";
import * as moment from "moment";
import { UploadedFile } from "express-fileupload";
import { conn } from "../config/Database";
import { Request } from "express";
import { ProductQueryEnum } from "../infrastructure/ProductQueryEnum";
import { invokeQuery } from "../infrastructure/QueryEnum";
import { ProductResponse } from "types/product_schema";
import { StockQueryEnum } from "../infrastructure/StockQueryEnum";
import { CategoryQueryEnum } from "../infrastructure/CategoryQueryEnum";

type ProductExcelRequest = {
    name: string;
    stock: string;
    category: string;
}

export const createProduct = (product: ProductResponse, callback: Function) => {
    conn.query(invokeQuery('INSERT', 'Product', ProductQueryEnum.INSERT),
        [product.name, product.createdAt, product.idStock, product.idCategory, product.idEmployee], (error: any, results: any) => {
            if (error) return callback(error);

            const productId: number = results['insertId'];

            return callback(null, productId);
        });
}

export const getProducts = (callback: Function) => {
    conn.query(invokeQuery('SELECT', null, ProductQueryEnum.SELECT), (error: any, results: any) => {
        if (error) return callback(error);

        const products: ProductResponse[] = results;

        return callback(null, products);
    });
}

export const getProduct = (productId: number, callback: Function) => {
    conn.query(invokeQuery('SELECT', null, ProductQueryEnum.SELECT_BY_ID), [productId], (error: any, results: any) => {
        if (error) return callback(error);

        return callback(null, results[0]);
    });
}

export const importExcelProduct = (req: Request, callback: Function) => {
    const product_xlsx = req.files!.product_xlsx as UploadedFile;
    const path = `${process.cwd()}/src/upload/providers/${product_xlsx.name}`;

    product_xlsx.mv(path, async (error: any) => {
        if (error) callback(error);

        const workbook = xlsx.readFile(path);

        const sheet_name_list = workbook.SheetNames;

        const worksheet = workbook.Sheets[sheet_name_list[0]];

        const worksheet_to_json = xlsx.utils.sheet_to_json(worksheet) as ProductExcelRequest[];

        const worksheet_clean = await Promise.all(worksheet_to_json.map(async (product: ProductExcelRequest) => {
            const idStock = await syncStock(product.stock);
            const idCategory = await syncCategory(product.category);

            return {
                name: product.name,
                createdAt: moment().unix(),
                idStock: idStock,
                idCategory: idCategory,
                idEmployee: req.sub
            } as ProductResponse;
            
        })) as ProductResponse[];

        bulkInsertProducts(worksheet_clean);

        return callback(null, "imported");
    });
}

export const syncProducts = () => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', null, ProductQueryEnum.SELECT), (error: any, results: any) => {
            if (error) return reject(error);

            return resolve(results[0].id);
        });
    });
}

export const syncStock = (title: string) => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', null, StockQueryEnum.SELECT_BY_NAME), [title], (error: any, results: any) => {
            if (error) return reject(error);

            return resolve(results[0].id);
        });
    });
}

export const syncCategory = (title: string) => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', null, CategoryQueryEnum.SELECT_BY_NAME), [title], (error: any, results: any) => {
            if (error) return reject(error);

            return resolve(results[0].id);
        });
    });
}

export const bulkInsertProducts = (products: ProductResponse[]) => {
    conn.query(invokeQuery('INSERT', 'Product', ProductQueryEnum.MULTIPLE_INSERT),
        [products.map((product: ProductResponse) =>
            [product.name, product.createdAt, product.idStock, product.idCategory,
            product.idEmployee])]);
}

export const updateProductStock = (stock:number, productId: number) => {
    conn.query(invokeQuery('UPDATE', null, ProductQueryEnum.UPDATE_STOCK), [stock, productId]);
}

export const updateProductPrice = (price:number, productId: number) => {
    conn.query(invokeQuery('UPDATE', null, ProductQueryEnum.UPDATE_PRICE), [price, productId]);
}