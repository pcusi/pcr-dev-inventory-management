import { countCustomer } from "./CustomerController";
import { countKardexByEntry, countKardexByExit } from "./KardexController";
import { countProvider } from "./ProviderController";

export const report = async (callback: Function) => {
    const providers = await countProvider();
    const customers = await countCustomer();
    const kardex_entry = await countKardexByEntry();
    const kardex_exit = await countKardexByExit();

    const response: object = {
        providers: {
            title: "Proveedores",
            count: providers
        },
        customers: {
            title: "Clientes",
            count: customers
        },
        kardex_entry: {
            title: "Entradas",
            count: kardex_entry
        },
        kardex_exit: {
            title: "Salidas",
            count: kardex_exit
        }
    }

    return callback(null, response);
}