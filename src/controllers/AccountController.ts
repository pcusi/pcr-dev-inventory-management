import { EmployeeResponse } from "types/employee_schema";
import { conn } from "../config/Database";
import * as bcrypt from "bcrypt";
import { invokeQuery } from "../infrastructure/QueryEnum";
import * as jwt from "jsonwebtoken";
import { PayloadResponse } from "types/payload_schema";
import { JWTEnum } from "../infrastructure/JWTEnum";
import { EmployeeQueryEnum } from "../infrastructure/EmployeeQueryEnum";

export const logIn = (employee: EmployeeResponse, callback: Function) => {
  conn.query(
    invokeQuery("SELECT", null, EmployeeQueryEnum.SELECT_BY_USERNAME),
    [employee.email],
    async (error: any, results: any) => {
      if (error) return callback(error);

      if (results.length === 0) return callback(null, "not-found");

      const password: string = results[0].password;

      delete results[0].password;

      const employee_response: EmployeeResponse = {
        ...results[0],
      };

      const token = await createToken(
        employee.password!,
        password,
        employee_response
      );

      if (token === "") return callback(null, "not-match");

      return callback(null, token);
    }
  );
};

async function createToken(
  password: string,
  hashPassword: string,
  employee?: EmployeeResponse
): Promise<string> {
  const isCorrect = await bcrypt.compare(password, hashPassword);

  delete employee?.id;

  const payload: PayloadResponse = {
    sub: employee?.id,
    employee: employee,
  };

  let token: string = "";

  isCorrect
    ? (token = jwt.sign(payload, JWTEnum.SECRET, {
        expiresIn: process.env.TOKEN_EXPIRES,
      }))
    : token;

  return token;
}

export const getUser = (token: string, callback: Function) => {
  const employee = jwt.decode(token);

  return callback(null, employee);
};
