import { conn } from "../config/Database";
import { StockResponse } from "types/stock_schema";
import { invokeQuery } from "../infrastructure/QueryEnum";
import { StockQueryEnum } from "../infrastructure/StockQueryEnum";

export const createStock = (stock: StockResponse, callback: Function) => {
    conn.query(invokeQuery('INSERT', 'Stock', StockQueryEnum.INSERT), [stock.name], (error: any, results: any) => {
        if (error) return callback(error);

        const categoryId: number = results['insertId'];

        return callback(null, categoryId);
    });
}

export const getStocks = (callback: Function) => {
    conn.query(invokeQuery('SELECT', null, StockQueryEnum.SELECT), (error: any, results: any) => {
        if (error) return callback(error);

        const stocks: StockResponse[] = results;

        return callback(null, stocks);
    });
}

export const getStock = (stockId: number, callback: Function) => {
    conn.query(invokeQuery('SELECT', null, StockQueryEnum.SELECT_BY_ID),
        [stockId], (error: any, results: any) => {
            if (error) return callback(error);

            return callback(null, results[0]);
        });
}