import { conn } from "../config/Database";
import { KardexResponse } from "types/kardex_schema";
import { invokeQuery } from "../infrastructure/QueryEnum";
import { KardexQueryEnum } from "../infrastructure/KardexQueryEnum";
import { ProductQueryEnum } from "../infrastructure/ProductQueryEnum";

export const createKardex = (kardex: KardexResponse, callback: Function) => {
    conn.query(invokeQuery('INSERT', 'Kardex', KardexQueryEnum.INSERT),
        [kardex.kardexCode, kardex.kardexType, kardex.kardexOrder,
        kardex.kardexTotal, kardex.kardexDate, kardex.idEmployee], (error: any, results: any) => {
            if (error) return callback(error);

            const kardexId: number = results['insertId'];

            return callback(null, kardexId);
        });
}

export const getKardexes = (callback: Function) => {
    conn.query(invokeQuery('SELECT', null, KardexQueryEnum.SELECT), (error: any, results: any) => {
        if (error) return callback(error);

        return callback(null, results[0]);
    });
}

export const getKardex = (kardexId: number, callback: Function) => {
    conn.query(invokeQuery('SELECT', null, KardexQueryEnum.SELECT_BY_ID),
        [kardexId], (error: any, results: any) => {
            if (error) return callback(error);

            return callback(null, results[0]);
        });
}

export const countKardex = () => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', null, KardexQueryEnum.COUNT), (error: any, results: any) => {
            if (error) reject(error);

            resolve(results[0].count);
        });
    });
}

export const countKardexByEntry = () => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', null, KardexQueryEnum.COUNT_BY_ENTRY), (error: any, results: any) => {
            if (error) reject(error);

            resolve(results[0].count);
        });
    });
}

export const countKardexByExit = () => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', null, KardexQueryEnum.COUNT_BY_EXIT), (error: any, results: any) => {
            if (error) reject(error);

            resolve(results[0].count);
        });
    });
}

export const syncKardexProduct = (productId: number) => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', null, ProductQueryEnum.SELECT), [productId], (error: any, results: any) => {
            if (error) reject(error);

            resolve(results[0]);
        });
    });
}