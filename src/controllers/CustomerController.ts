import { conn } from "../config/Database";
import { CustomerQueryEnum } from "../infrastructure/CustomerQueryEnum";
import { invokeQuery } from "../infrastructure/QueryEnum";
import { CustomerResponse } from "types/customer_schema";

export const createCustomer = (customer: CustomerResponse, callback: Function) => {
    conn.query(invokeQuery('INSERT', 'Customer', CustomerQueryEnum.INSERT),
        [customer.names, customer.lastnames, customer.documentType, customer.documentNumber,
        customer.email, customer.createdAt, customer.idEmployee], (error: any, results: any) => {
            if (error) callback(error);

            const customerId: number = results.insertId;

            callback(null, customerId);
        });
}

export const getCustomers = (callback: Function) => {
    conn.query(invokeQuery('SELECT', null, CustomerQueryEnum.SELECT), (error: any, results: any) => {
        if (error) callback(error);

        callback(null, results);
    });
}

export const countCustomer = () => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', null, CustomerQueryEnum.COUNT), (error: any, results: any) => {
            if (error) reject(error);

            resolve(results[0].count);
        });
    });
}