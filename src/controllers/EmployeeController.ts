import { conn } from "../config/Database";
import { EmployeeResponse } from "types/employee_schema";
import { EmployeeQueryEnum } from "../infrastructure/EmployeeQueryEnum";
import { invokeQuery } from "../infrastructure/QueryEnum";
import * as moment from 'moment';

export const createEmployee = (employee: EmployeeResponse, callback: Function) => {
    employee.createdAt = moment().unix();

    conn.query(
        invokeQuery('INSERT', 'Employee', EmployeeQueryEnum.INSERT),
        [employee.role, employee.names, employee.lastnames, employee.documentType,
            employee.documentNumber, employee.email, employee.password, employee.createdAt],
        (error: any, results: any) => {
            if (error) return callback(error);

            const employeeId: number = results['insertId'];

            return callback(null, employeeId);
        });
}

export const getEmployees = (callback: Function) => {
    conn.query(invokeQuery('SELECT', null, EmployeeQueryEnum.SELECT), (error: any, results: any) => {
        if (error) return callback(error);

        const employees: EmployeeResponse[] = results;

        const employees_format = employees.map((employee: EmployeeResponse) => {
            return {
                id: employee.id,
                role: employee.role,
                names: employee.names,
                lastnames: employee.lastnames,
                documentType: employee.documentType,
                documentNumber: employee.documentNumber,
                email: employee.email,
                status: employee.status,
                createdAt: moment(employee.createdAt! * 1000).format('DD/MM/YYYY')
            }
        });

        return callback(null, employees_format);
    });
}

export const getEmployee = (employeeId: number, callback: Function) => {
    conn.query(invokeQuery('SELECT', null, EmployeeQueryEnum.SELECT_BY_ID), [employeeId], (error: any, results: any) => {
        if (error) return callback(error);

        const employee: EmployeeResponse = results[0];

        delete employee.password;

        return callback(null, employee);
    });
}

export const updateEmployee = (employeeId: number, employee: EmployeeResponse, callback: Function) => {
    employee.updatedAt = moment().unix();
    
    conn.query(
        invokeQuery('UPDATE', 'Employee', EmployeeQueryEnum.UPDATE),
        [employee.names, employee.lastnames, employee.documentType,
            employee.documentNumber, employee.email, employee.password, employee.updatedAt, employeeId],
        (error: any, message: string) => {
            if (error) return callback(error);

            message = 'Employee updated successfully';

            return callback(null, message);
        });
}