import * as xlsx from 'xlsx';
import { ProviderResponse } from "types/provider_schema";
import { Request } from "express";
import { conn } from "../config/Database";
import { invokeQuery } from "../infrastructure/QueryEnum";
import { ProviderQueryEnum } from "../infrastructure/ProviderQueryEnum";
import { UploadedFile } from "express-fileupload";
import { ExcelFileMessageEnum } from '../infrastructure/ExcelFileMessageEnum';

export const createProvider = async (provider: ProviderResponse, callback: Function) => {
    conn.query(invokeQuery('INSERT', 'Provider', ProviderQueryEnum.INSERT),
        [provider.names, provider.lastnames, provider.documentType, provider.documentNumber,
        provider.email, provider.createdAt, provider.idEmployee], (error: any, results: any) => {
            if (error) return callback(error);

            const providerId: number = results['insertId'];

            return callback(null, providerId);
        });
}

export const getProvider = async (providerId: number, callback: Function) => {
    conn.query(invokeQuery('SELECT', null, ProviderQueryEnum.SELECT_BY_ID),
        [providerId], (error: any, results: any) => {
            if (error) return callback(error);

            return callback(null, results[0]);
        });
}

export const importExcelProvider = (req: Request, callback: Function) => {
    const provider_xlsx = req.files!.provider_xlsx as UploadedFile;

    const path = `${process.cwd()}/src/upload/providers/${provider_xlsx.name}`;

    provider_xlsx.mv(path, (error) => {
        if (error) return callback(error);

        return callback(null, provider_xlsx.name);
    });
}

export const exportExcelProvider = async (callback: Function) => {
    const sync_providers = await getProviders() as ProviderResponse[];

    const providers: ProviderResponse[] = [...sync_providers].map((provider: ProviderResponse) => {
        return {
            documentType: provider.documentType,
            documentNumber: provider.documentNumber,
            names: provider.names,
            email: provider.email
        }
    });

    const workbook = xlsx.utils.book_new();

    const worksheet: xlsx.WorkSheet = xlsx.utils.json_to_sheet([]);

    const headers = [["Tipo de Documento", "Nro de Documento", "Dirección", "Correo"]];

    xlsx.utils.sheet_add_aoa(worksheet, headers);

    xlsx.utils.sheet_add_json(worksheet, providers, { origin: 'A2', skipHeader: true });

    xlsx.utils.book_append_sheet(workbook, worksheet, 'providers');

    xlsx.writeFile(workbook, `${process.cwd()}/src/upload/providers/providers.xlsx`);

    return callback(null, ExcelFileMessageEnum.EXPORTED);
}

export const countProvider = () => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', 'Provider', ProviderQueryEnum.COUNT), (error: any, results: any) => {
            if (error) reject(error);

            resolve(results[0].count);
        });
    });
}

export const getProviders = () => {
    return new Promise((resolve, reject) => {
        conn.query(invokeQuery('SELECT', null, ProviderQueryEnum.SELECT), (error: any, results: any) => {
            if (error) reject(error);

            resolve(results);
        });
    });
}