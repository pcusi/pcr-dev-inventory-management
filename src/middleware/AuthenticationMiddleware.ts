import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { JWTEnum } from "../infrastructure/JWTEnum";

export const auth = (req: Request, res: Response, next: NextFunction) => {
  const authorization = req.headers.authorization;

  if (!authorization) return res.status(401).send({ message: "Unauthorized." });

  jwt.verify(authorization, JWTEnum.SECRET, (error: any, account: any) => {
    if (error) return res.status(403).send({ message: "Token expired." });
    req.sub = account.sub;
    next();
  });
};
