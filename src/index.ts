import * as dotenv from 'dotenv';
dotenv.config({ path: `${process.cwd()}/.env` });
import { conn } from "./config/Database";
import * as express from 'express';
import * as expressUpload from 'express-fileupload';
import * as cors from 'cors';
import { PortEnum } from './infrastructure/PortEnum';
import { FileArray } from "express-fileupload";

const app = express();

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(expressUpload());
app.use(cors());

//routes
import * as accountRoutes from './routes/AccountRoutes';
import * as categoryRoutes from './routes/CategoryRoutes';
import * as customerRoutes from './routes/CustomerRoutes';
import * as dashboardRoutes from './routes/DashboardRoutes';
import * as employeeRoutes from './routes/EmployeeRoutes';
import * as kardexRoutes from './routes/KardexRoutes';
import * as providerRoutes from './routes/ProviderRoutes';
import * as productRoutes from './routes/ProductRoutes';
import * as stockRoutes from './routes/StockRoutes';

const routes: express.Router[] = [
    employeeRoutes.router,
    accountRoutes.router,
    providerRoutes.router,
    productRoutes.router,
    categoryRoutes.router,
    stockRoutes.router,
    kardexRoutes.router,
    customerRoutes.router,
    dashboardRoutes.router,
]

app.use('/api/v1', [...routes]);

app.listen(PortEnum.PORT, () => {
    conn.connect((error, connection) => {
       if (error) throw error;
       console.log(`MYSQL connection successfully`);
    });
    console.log(`Server is listening on port ${PortEnum.PORT}`);
});

declare global {
    namespace Express {
        interface Request {
            sub: number,
            files?: FileArray
        }
    }
}