CREATE DATABASE IF NOT EXISTS inventory_db;

USE inventory_db;

CREATE TABLE Employee(
    id INT NOT NULL AUTO_INCREMENT,
    names varchar(50) NOT NULL,
    lastnames varchar(50) NOT NULL,
    role varchar(25) NOT NULL,
    documentType varchar(20) NOT NULL,
    documentNumber varchar(8) NOT NULL,
    email varchar(30) NULL,
    password varchar(200) NULL,
    status tinyint default 1,
    createdAt int,
    updatedAt int,
    PRIMARY KEY(id)
);

CREATE TABLE Provider(
    id INT NOT NULL AUTO_INCREMENT,
    names varchar(50) NOT NULL,
    lastnames varchar(50) NOT NULL,
    documentType varchar(20) NOT NULL,
    documentNumber varchar(8) NOT NULL,
    email varchar(30) NOT NULL,
    status tinyint default 1,
    createdAt int,
    updatedAt int,
    idEmployee int,
    PRIMARY KEY(id),
    FOREIGN KEY (idEmployee) REFERENCES Employee(id)
);

CREATE TABLE Customer(
    id INT NOT NULL AUTO_INCREMENT,
    names varchar(50) NOT NULL,
    lastnames varchar(50) NOT NULL,
    documentType varchar(20) NOT NULL,
    documentNumber varchar(8) NOT NULL,
    email varchar(30) NOT NULL,
    status tinyint default 1,
    createdAt int,
    updatedAt int,
    idEmployee int,
    PRIMARY KEY(id),
    FOREIGN KEY (idEmployee) REFERENCES Employee(id)
);

CREATE TABLE Stock(
    id INT NOT NULL AUTO_INCREMENT,
    name varchar(50) not null,
    PRIMARY KEY(id)
);

CREATE TABLE Category(
    id INT NOT NULL AUTO_INCREMENT,
    name varchar(50) not null,
    PRIMARY KEY(id)
);

CREATE TABLE Product(
    id INT NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    description varchar(50) NULL,
    stock int default 0,
    price decimal(10, 2) NULL,
    image varchar(250) NULL,
    status tinyint default 1,
    createdAt int,
    updatedAt int,
    idStock int,
    idEmployee int,
    idCategory int,
    PRIMARY KEY(id),
    FOREIGN KEY (idStock) REFERENCES Stock(id),
    FOREIGN KEY (idEmployee) REFERENCES Employee(id),
    FOREIGN KEY (idCategory) REFERENCES Category(id)
);

CREATE TABLE Kardex(
    id INT NOT NULL AUTO_INCREMENT,
    kardexCode VARCHAR(50) not null,
    kardexType ENUM('Entrada', 'Salida') not null,
    kardexOrder JSON,
    kardexTotal DECIMAL(10,2) not null,
    status varchar(25) default "P",
    kardexDate int,
    idEmployee int,
    PRIMARY KEY (id),
    FOREIGN KEY (idEmployee) REFERENCES Employee(id)
);

CREATE TABLE Sales(
    id INT NOT NULL AUTO_INCREMENT,
    saleOrder JSON,
    saleTotal DECIMAL(10,2) not null,
    saleDate int,
    status varchar(25) default "P",
    idCustomer int,
    idEmployee int,
    PRIMARY KEY (id),
    FOREIGN KEY (idCustomer) REFERENCES Customer(id),
    FOREIGN KEY (idEmployee) REFERENCES Employee(id)
);